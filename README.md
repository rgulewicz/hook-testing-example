![](hook.jpeg)

# 1. Mocked approach
(branch: `mocked-approach`)

The hook in question is automocked by jest. Its mock implentation sits alongside the actual implementation (in `hooks/useWindowWidth/testUtils.ts`) and is then re-exported from `__mocks__/hooks/useWindowWidth.ts`. This way, you avoid scattering closely related code across the project.

Any time a module being tested makes use of the hook, jest will automagically use this mock instead of the actual hook, without you doing anything. Hurray! 🎉

The mock implementation makes use of a simple event emitter and a couple of helper functions, all sitting in `hooks/useWindowWidth/testUtils.ts`. You can use these to simulate any behavior inside the hook that you may need.

Sure, in a simple case like reading the window width, this is a complete and utter overkill but can be quite useful in more complex scenarios.

# 2. Non-mock approach
(branch: `no-mocks-please`)

Any components being tested use the actual hook implementation, and the hook itself is not mocked at all anywhere in the project.

However, this means that triggering whatever it is that causes the hook to do its work may require some ugly solutions, especially if we're talking JSDOM-related events. To keep tests as clean as possible, you can still use a couple of helpers to keep those hacks hidden away. In our case, they can be found in `hooks/useWindowWidth/testUtils.ts`.
