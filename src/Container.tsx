import { useWindowWidth } from 'hooks';

function Container({
  aspectRatio
}: {
  aspectRatio: `${number}:${number}`
}): JSX.Element {
  const [ratioW, ratioH] = aspectRatio.split(':').map(Number);
  const width = useWindowWidth();
  const height = width * (ratioH / ratioW);
  return (
    <main
      style={{
        width: `${width}px`,
        height: `${height}px`
      }}
    />
  );
}

export default Container;
