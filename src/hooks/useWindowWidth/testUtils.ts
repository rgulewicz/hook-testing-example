/* eslint-disable react-hooks/rules-of-hooks */
import { useEffect, useState } from 'react';
import { act } from '@testing-library/react';
import EventEmitter from 'events';

import useWindowWidth from './useWindowWidth';

type TheRealThing = typeof useWindowWidth;

interface TheMock {
  // ensure the mock's signature is always in sync with the actual function
  (...args: Parameters<TheRealThing>): ReturnType<TheRealThing>;
  triggerChange: (nextWidth: number) => void;
  reset: () => void;
}

const INITIAL_WIDTH = 1024;
const WIDTH_EVENT = Symbol();

// This could just as well be any other kind of observable value (eg. an RxJS Subject)
const emitter = new EventEmitter();

export const mockUseWindowWidth: TheMock = () => {
  const [width, setWidth] = useState(INITIAL_WIDTH);
  useEffect(() => {
    emitter.addListener(WIDTH_EVENT, setWidth);
    return () => {
      emitter.removeListener(WIDTH_EVENT, setWidth);
    };
  }, []);
  return width;
};

mockUseWindowWidth.triggerChange = (nextWidth) => act(() => {
  emitter.emit(WIDTH_EVENT, nextWidth);
});


mockUseWindowWidth.reset = () => act(() => {
  emitter.emit(WIDTH_EVENT, INITIAL_WIDTH);
});
