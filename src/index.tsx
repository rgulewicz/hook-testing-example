import React from 'react';
import ReactDOM from 'react-dom';
import App from './Container';

ReactDOM.render(
  <React.StrictMode>
    <App aspectRatio="1:2" />
  </React.StrictMode>,
  document.getElementById('root')
);
