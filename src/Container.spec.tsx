import { render, screen } from '@testing-library/react';
import Container from './Container';

import { mockUseWindowWidth } from 'hooks/useWindowWidth/testUtils';

describe('<Container />', () => {
  afterEach(() => {
    mockUseWindowWidth.reset();
  });

  it('stretches width to window size', async () => {
    render(<Container aspectRatio="16:9" />);
    const container = screen.getByRole('main');

    mockUseWindowWidth.triggerChange(4096);
    expect(container).toHaveStyle('width: 4096px');
  });

  it('maintains aspect ratio', async () => {
    render(<Container aspectRatio="16:9" />);
    const container = screen.getByRole('main');

    mockUseWindowWidth.triggerChange(1600);
    
    expect(container).toHaveStyle('width: 1600px');
    expect(container).toHaveStyle('height: 900px');
  });
});
