// Simply re-export the mocked version of the function. This way, the actual mock implementation
// and any test helpers stay right next to the non-mock implementation. Nice and tidy :)

export { mockUseWindowWidth as default } from 'hooks/useWindowWidth/testUtils';
